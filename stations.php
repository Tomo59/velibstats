<?php
require_once('config.php');
include_once('functions.php');

//Filtre 24 heures
$hier = new DateTime("-10day");
$filtreDate = $hier->format('Y-m-d H:i:s');

//Dernière conso
$requete = $pdo->query('SELECT * FROM `statusConso` where nbStation Is not null and date >= "'.$filtreDate.'" Order by id desc limit 0,1');
$conso = $requete->fetch();

//Stations
if(is_null($conso['id']))
    $statusStation = array();
else
{
    $requete = $pdo->query('SELECT * FROM status inner join `stations` on stations.code = status.code WHERE idConso = '.$conso['id'].' order by status.code asc');
    $statusStation = $requete->fetchAll();
}
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Vélib Stats (site non officiel)</title>
		<?php include('head_info.html'); ?>
	</head>
	<body>

		<?php generateNavBar("Stations"); ?>

		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-6 pt-3">
					<div id="mapid" style="width: 100%; height: 800px;"></div>
				</div>
				<div class="col-xl-6 d-flex align-items-center h100">
					<div id="stationDetails" style="width: 100%;"><h1 class="text-center">Cliquez sur une station pour avoir plus d'informations</h1></div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<h2>Liste des stations</h2>
					<table id="stations" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Code</th>
								<th>Nom</th>
								<th>Date d'ouverture</th>
								<th>Statut</th>
								<th>Vélos mécaniques dispo</th>
								<th>Vélos électriques dispo</th>
								<th>Bornes libres</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($statusStation as $station)
							{
								echo '<tr>';
								echo '<td><a href="station.php?code='.$station['code'].'">'.displayCodeStation($station['code']).'</a></td>';
								echo '<td>'.$station['name'].'</td>';
								echo '<td>'.$station['dateOuverture'].'</td>';
								echo '<td>'.(($station['state'] == 'Operative' && $station['nbEDock'] != 0) ? 'Ouverte' : 'En travaux').'</td>';
								echo '<td>'.$station['nbBike'].'</td>';
								echo '<td>'.$station['nbEBike'].'</td>';
								echo '<td>'.$station['nbFreeEDock'].'/'.$station['nbEDock'].'</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<?php include('footer.html'); ?>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>

