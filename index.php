<?php
require_once('config.php');
include_once('functions.php');

//Filtre 24 heures
$hier = new DateTime("-10day");
$filtreDate = $hier->format('Y-m-d H:i:s');

//Id of last meaningful entry in statusConso
$request = $pdo->query('SELECT * FROM `statusConso` where nbStation Is not null and date >= "'.$filtreDate.'" Order by id desc limit 0,1');
$lastinfo = $request->fetch();

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Vélib Stats (site non officiel)</title>
        <?php include('head_info.html'); ?>
    </head>
    <body>

      <?php generateNavBar("Accueil"); ?>

      <div class="container-fluid">
        <div class="row">
          <div class="col text-center border border-success m-4">
            <h2>Stations<br /><?php echo $lastinfo['nbStation']; ?></h2>
          </div>
          <div class="col text-center border border-success m-4">
            <h2>Vélos mécaniques<br /><?php echo $lastinfo['nbBike']; ?></h2>
          </div>
          <div class="col text-center border border-success m-4">
            <h2>Vélos électriques<br /><?php echo $lastinfo['nbEbike']; ?></h2>
          </div>
          <div class="col text-center border border-success m-4">
            <h2>Bornes libres<br /><?php echo $lastinfo['nbFreeEDock']; ?></h2>
          </div>
          <div class="col text-center border border-success m-4">
            <h2>Bornes total<br /><?php echo $lastinfo['nbEDock']; ?></h2>
          </div>
        </div>
        
        <div class="row mb-4">
          <div class="col-xl-6 p-4">
            <div id="chartNbStations" style="height:600px;"></div>
          </div>
          <div class="col-xl-6 p-4">
            <div id="chartNbVelib" style="height:600px;"></div>
          </div>
        </div>
      </div>

      <?php include('footer.html'); ?>
      <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
