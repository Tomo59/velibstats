<?php
require_once('config.php');
include_once('functions.php');

if(!isset($_GET['code']) || intval($_GET['code']) <= 0)
{
	header('location: stations.php');
	exit();
}

//Get the station
$code = intval($_GET['code']);
$request = $pdo->query('SELECT * FROM stations WHERE code = '.$code);
if($request === false)
{
	header('location: index.php');
	exit();
}
$station = $request->fetch();
if($station === false)
{
	header('location: index.php');
	exit();
}

//Filtre 24 heures
$hier = new DateTime("-1day");
$filtreDate = $hier->format('Y-m-d H:i:s');

//Id of last meaningful entry in statusConso
$request = $pdo->query('SELECT * FROM `statusConso` where nbStation Is not null and date >= "'.$filtreDate.'" Order by id desc limit 0,1');
$lastinfo = $request->fetch();

// Station last info
$request = $pdo->query('SELECT s.nbBike, s.nbEBike, s.nbFreeEDock, s.nbEDock
	FROM `status` s
	WHERE s.code = '.$code.' AND s.idConso = '.$lastinfo['id']);
$lastStatus = $request->fetch();

// For the stats section
//Stations
$request = $pdo->query('SELECT c.date, s.nbBike, s.nbEBike, s.nbFreeEDock, s.nbEDock
	FROM `status` s
	INNER JOIN statusConso c ON c.id = s.idConso
	WHERE s.code = '.$code.' AND c.date >= "'.$filtreDate.'"
	ORDER BY s.code ASC');
$stationHistory = $request->fetchAll();

$address = getAddressFromCoord($station['latitude'], $station['longitude']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Vélib Stats → Station (site non officiel)</title>
	<?php include('head_info.html'); ?>
</head>
<body>

	<?php generateNavBar(""); ?>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm text-center">
				<p class='lead'>Station
					<?=displayCodeStation($code)?> -
					<small class="text-muted"><?=$station['name']?></small>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col text-left">
				<p class='small'>Adresse la plus proche (selon <a href="https://adresse.data.gouv.fr/">BAN</a>) : <?=$address?></p>
			</div>
			<div class="col-lg-4"></div>
		</div>
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col text-left">
				<div>
					<b>État le <?=$lastinfo['date']?> :</b>
					<ul>
						<li>Vélos mécaniques : <?php echo $lastStatus['nbBike']; ?></li>
						<li>Vélos électriques : <?php echo $lastStatus['nbEBike']; ?></li>
						<li>Bornes libres : <?php echo $lastStatus['nbFreeEDock']; ?></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4"></div>
		</div>

		<h4>État de la station</h4>

		<div class="row">
			<div class="col">
				<h6>Dernières 6 heures<br/></h6>
				<img src="/images/<?=$code?>_6hours.png"></img>
			</div>
			<div class="col">
				<h6>Dernier jour<br/></h6>
				<img src="/images/<?=$code?>_1day.png"></img>
			</div>
			<div class="col">
				<h6>Dernière semaine<br/></h6>
				<img src="/images/<?=$code?>_1week.png"></img>
			</div>
			<div class="col">
				<h6>Dernier mois<br/></h6>
				<img src="/images/<?=$code?>_1month.png"></img>
			</div>
			<div class="col">
				<h6>Dernière année<br/></h6>
				<img src="/images/<?=$code?>_1year.png"></img>
			</div>
			<div class="col">
				<h6>Historique complet<br/></h6>
				<img src="/images/<?=$code?>_full.png"></img>
			</div>
		</div>

		<h4>Stats</h4>
		<table id="stats" class="table table-bordered">
		    <thead>
			<tr>
				<th>Date</th>
				<th>Vélos mécaniques dispo</th>
				<th>Vélos électriques dispo</th>
				<th>Bornes libres</th>
			</tr>
			</thead>
			<tbody>
				<?php
				//On définie les points pour les graphs
				foreach($stationHistory as $row)
				{
					echo '<tr>';
					echo '<td>'.$row['date'].'</td>';
					echo '<td>'.$row['nbBike'].'</td>';
					echo '<td>'.$row['nbEBike'].'</td>';
					echo '<td>'.$row['nbFreeEDock'].'/'.$row['nbEDock'].'</td>';
					echo '</tr>';
				}
				?>
			</tbody>
		</table>
		<script type="application/javascript">
			$(document).ready(
				function () {
					$('#stats').DataTable({language: dtTraduction});
				} );
		</script>
	</div>

	<?php include('footer.html'); ?>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
