<?php

function displayCodeStation($code)
{
    if($code < 10000)
        return '0'.$code;
    return $code;
}

function getAddressFromCoord($lat, $lon)
{
	// create curl resource
	$ch = curl_init();

	// set url
	$urlBAN = 'https://api-adresse.data.gouv.fr/reverse/?lat='.$lat.'&lon='.$lon;
	curl_setopt($ch, CURLOPT_URL, $urlBAN);

	//return the transfer as a string
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	// $output contains the output string
	$output = curl_exec($ch);

	// close curl resource to free up system resources
	curl_close($ch);

	$tmp = json_decode($output, true);
	return $tmp['features']['0']['properties']['label'];
}

function generateNavBar($current)
{
?>
	<header class="navbar navbar-expand-lg navbar-dark bg-success">
		<a class="navbar-brand" href="/">Velib.tomobox.fr</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarContent">
			<ul class="navbar-nav mr-auto">
			<li class="nav-item <?php if($current=="Accueil") print "active"?>" id="nav-item-Accueil">
					<a class="nav-link" href="/index.php">Accueil
					<?php if($current=="Stations") print ' <span class="sr-only">(current)</span>' ?>
					</a>
				</li>
				<li class="nav-item <?php if($current=="Stations") print "active"?>" id="nav-item-Stations">
					<a class="nav-link" href="/stations.php">Stations
					<?php if($current=="Stations") print ' <span class="sr-only">(current)</span>' ?>
					</a>
				</li>
			</ul>
		</div>
	</header>
<?php
}


?>
