Here are the instructions to have a working website on your localhost machine.


# Setup MySQL

```bash
sudo apt install mysql-server mysql-client
sudo mysql_secure_installation

mysql -u root -p
> CREATE DATABASE velib;
> GRANT ALL PRIVILEGES ON velib.* TO 'www-data'@'localhost' IDENTIFIED BY '<YOUR_PASSWORD>';
> quit
```

Create structure and possibly add real data

```bash
mysql -uwww-data -p -D velib
> source structure.sql;
> source dump_velib.sql;
```

# Setup Nginx and PHP

```bash
sudo apt install php-mysql php-fpm nginx-full

sudo systemctl enable nginx
sudo systemctl enable php-fpm
sudo systemctl start nginx
sudo systemctl start php-fpm
```

Copy the following content in /etc/nginx/sites-available/velib.conf and adapt root and fastcgi_pass:

    server {
        listen 80 default_server;
        listen [::]:80 default_server;
     
        root /home/tomo/workspace/velibstats;
     
        # Add index.php to the list if you are using PHP
        index index.php index.html index.htm index.nginx-debian.html;
     
        server_name localhost.velib;
     
        location / {
            # First attempt to serve request as file, then
            # as directory, then fall back to displaying a 404.
            try_files $uri $uri/ =404;
        }
     
        # pass PHP scripts to FastCGI server
        #
        location ~ \.php$ {
                    fastcgi_pass unix:/run/php/php7.1-fpm.sock;
                    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                    include fastcgi_params;
                    include snippets/fastcgi-php.conf;
        }
     
        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #   deny all;
        #}
    }


Then enable the site :
```bash
cd /etc/nginx/sites-enabled
sudo ln -s /etc/nginx/sites-available/velib.conf

sudo systemctl reload nginx
```

# Final setup

Edit /etc/hosts to put "127.0.0.2 localhost.velib"

Create config.php from config.example.php with correct host, database name and user:password.
