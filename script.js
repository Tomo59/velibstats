function getData(url)
{
    return new Promise(
        function(resolve, reject)
        {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.onload = function()
            {
                if (xhr.status === 200)
                    resolve(JSON.parse(xhr.response));
                else
                    reject(xhr.statusText);
            };

            xhr.send();
        }
    );
}

var graphInstance = {};
var graphData = null;
var graphDataSets = null;
var map;

function displayGraph(chartID)
{
	var graphdata;
	var actionUrl;

	graphdata = {
		theme: 'light2',
		animationEnabled: false,
		zoomEnabled: true,
	};

	// prepare each graph
	switch (chartID) {
	case "chartNbStations":
		graphdata.title = { text: "Nombre de stations (zoom possible)" };
		graphdata.toolTip = {
			contentFormatter: function ( e ) {
				return e.entries[0].dataPoint.x.toLocaleString("fr-FR") + " : " +  e.entries[0].dataPoint.y;
			}
		};
		actionUrl = "getNbStation";
		break;

	case "chartNbVelib":
		graphdata.title = { text: "Nombre de vélos (zoom possible)" };
		graphdata.toolTip = { shared: true };
		actionUrl = "getDataVelib";
		break;

	default:
		console.warn('Error: No chart named ' + chartID + '.');
		return 1;
	}

	getData('api.php?action='+actionUrl).then(function(data)
	{
		// prepare the data with correct date
		data.forEach(function(data) {
			data["dataPoints"].forEach(function(el) {
				el.x = new Date(el.x);
			});
		});

		graphdata["data"] = data;

		CanvasGraph = new CanvasJS.Chart(chartID, graphdata);
		CanvasGraph.render();
	});
}

function initmap() {
    // set up the map
    map = new L.Map('mapid');

    // create the tile layer with correct attribution
    var osmUrl='https://velib.tomobox.fr/tiles/{z}/{x}/{y}.png';
    var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {minZoom: 10, maxZoom: 18, attribution: osmAttrib});

    // start the map in Paris
    map.setView(new L.LatLng(48.8566, 2.3522),12);
    map.addLayer(osm);
}

function populatemap() {
    getData('api.php?action=getStationsPos').then(function(data)
    {
        data["dataPoints"].forEach(function(el) {
            var marker = L.marker([el.latitude, el.longitude]).addTo(map);
            marker.bindPopup(
              "<b>Station " + el.code + "</b>&nbsp: " + el.name + "<br /><img width=400 src=\"/images/" + el.code + "_1day_small.png\"></img>",
              {
                maxWidth : 500
              }
            );
            L.DomEvent.addListener(marker, 'click', function(event){
                document.getElementById("stationDetails").innerHTML =
                "<h2 class=\"text-center\">Station " + this.code + "&nbsp: " + this.name + "</h2>" +
                "<p class=\"text-center\">Latitude&nbsp: " + this.latitude + "<br />" +
                "Longitude&nbsp: " + this.longitude + "</p>" +
                "<p>Graphique sur la dernière semaine&nbsp:" +
                "<img src=\"/images/" + this.code + "_1week.png\" style=\"width:100%;\" ></img>" +
                "<br />" +
                "<br />" +
                "<h4 class=\"text-center\"><a href=\"station.php?code=" + this.code + "\">Voir plus d'informations sur cette station</a></h4>";
            }, el);
        });
    });
}

var dtTraduction = {
    "sProcessing":     "Traitement en cours...",
    "sSearch":         "Rechercher&nbsp;:",
    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "sInfoPostFix":    "",
    "sLoadingRecords": "Chargement en cours...",
    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "oPaginate": {
        "sFirst":      "Premier",
        "sPrevious":   "Pr&eacute;c&eacute;dent",
        "sNext":       "Suivant",
        "sLast":       "Dernier"
    },
    "oAria": {
        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
    }
};

window.onload = function () {
    displayGraph("chartNbStations");
    displayGraph("chartNbVelib");
    initmap();
    populatemap();
    $('#stations').DataTable({
        "language": dtTraduction
    });
};

