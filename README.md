Abstract
========

This site provides a real-time tracking of the velib' stations openings, as well as the evolution of the bikes fleet.

This site and project isn't officiel, is not in any way linked to Vélib Métropole, the company offering the velib service ( http://www.velib-metropole.fr ).

INSTALLATION for LOCAL tests
============================

- install the web server, mysql and php
```
sudo apt install nginx mysql-client mysql-server php php-mysql php-curl
```

- configure `/etc/hosts` to have a URL for your site
```
127.0.0.1 localhost.velib
```

- configure the SQL database
```
sudo mysql -u root -p
> CREATE DATABASE velib;
> USE velib;
> SOURCE velib.sql;
> GRANT ALL PRIVILEGES ON velib.* TO 'username'@'localhost' IDENTIFIED BY 'password';
> \q;
```

- setup nginx to serve your website
  - Put the following in `/etc/nginx/sites-available/velib.conf` with two modifications:
    - Modify the path to the code in `root /home/tomo/...`
    - Modify the path to php if you don't have the 7.1 version: `unix:/run/php/php7.0-fpm.sock`.
```
# Velib website (in local, no HTTPS)
#
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /home/tomo/workspace/velibstats;

        # Add index.php to the list if you are using PHP
        index index.php index.html index.htm index.nginx-debian.html;

        server_name localhost.velib;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
        }

        # pass PHP scripts to FastCGI server
        #
        location ~ \.php$ {
                fastcgi_pass unix:/run/php/php7.1-fpm.sock;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
                include snippets/fastcgi-php.conf;
        }
}
```
- Tell nginx to use the `velib.conf` file and restart the web server: 
```
cd /etc/nginx/sites-enabled/
ln -s ../sites-available/velib.conf
rm -f default
systemctl restart nginx
```

- Create config.php in your source directory
```
cp config.example.php config.php
# adapt to:
# - setup the username and password (according to what you put in the SQL permission line)
# - database=velib, host=localhost
```

- navigate to http://localhost.velib



INSTALLATION of Open Street Map tile server
===========================================

- [install docker](https://docs.docker.com/install/)
- get the tileserver-gl image
```
docker pull klokantech/tileserver-gl
```
- get the mbtiles data you need. You can get this data easily from https://openmaptiles.com (but you need an account and you have outdated data if you don't pay). You can also convert a pbf file to mbtiles file (see below).
- run the docker image, you should be in the directory where you have your mbtiles data (note the `$(pwd)`), change the port 8081 to anything you like and is free
```
docker run -d -v $(pwd):/data -p 8081:80 klokantech/tileserver-gl
```
- prepare your nginx config to point to this running server (you can use any style installed in the docker, by default you have the choice between `osm-bright` and `klokantech-basic`)
```
location /tiles {
  proxy_pass http://localhost:8081/styles/osm-bright;
  proxy_redirect off;
  proxy_buffering off;
  proxy_set_header Host $host;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
}
```

