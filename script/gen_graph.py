#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import rrdtool

try:
    from os import fsencode, fsdecode
except:
    # fsencode/fsdecode are not available in python 2
    fsencode = str
    fsdecode = str

# RRD files location, TODO add an option to the script
rrd_path = "/home/tomo/workspace/velibstats/rrd"
png_path = "/home/tomo/workspace/velibstats/images"

def gen_graph(img_name, rrd_name, start_time, width, height, label):
    '''This function generates the image from the RRD file using the specified start time'''
    print("Generating {} from {} with start {}".format(img_name, rrd_name, start_time))
    try:
        rrdtool.graph(
            img_name,
            "--imgformat", "PNG",
            "--end", " now",
            "--start", start_time,
            "--width", str(width),
            "--height", str(height),
            "--vertical-label", label,
            "DEF:bike={}:bike:AVERAGE".format(rrd_name),
            "DEF:ebike={}:ebike:AVERAGE".format(rrd_name),
            "DEF:free_dock={}:free_dock:AVERAGE".format(rrd_name),
            "AREA:bike#8abf65:Velib mécaniques",
            "AREA:ebike#a2d1d9:Velib électriques:STACK",
            "AREA:free_dock#ffcd54:Bornes disponibles\c:STACK"
        )
    except rrdtool.OperationalError as err:
        print("OperationalError when generating {} : {}".format(img_name, err))
    except rrdtool.ProgrammingError as err:
        print("ProgrammingError when generating {} : {}".format(img_name, err))
    except Exception as err:
        print("ERROR:", err)


for filename in map(fsdecode, os.listdir(fsencode(rrd_path))):
    if filename.endswith(".rrd"):
        code = filename[:-4]
        filename_absolute = os.path.join(rrd_path, filename)
        if code == "total":
            height = 800
        else:
            height = 400
        print("Generating images for {}".format(filename_absolute))
        gen_graph(os.path.join(png_path, "{}_6hours.png".format(code)), filename_absolute, "end-6h", 800, height, "Station {} : 6 heures".format(code))
        gen_graph(os.path.join(png_path, "{}_1day.png".format(code)), filename_absolute, "end-1d", 800, height, "Station {} : 1 jour".format(code))
        gen_graph(os.path.join(png_path, "{}_1week.png".format(code)), filename_absolute, "end-1w", 800, height, "Station {} : 1 semaine".format(code))
        gen_graph(os.path.join(png_path, "{}_1month.png".format(code)), filename_absolute, "end-1m", 800, height, "Station {} : 1 mois".format(code))
        gen_graph(os.path.join(png_path, "{}_1year.png".format(code)), filename_absolute, "end-1y", 800, height, "Station {} : 1 an".format(code))
        gen_graph(os.path.join(png_path, "{}_full.png".format(code)), filename_absolute, "20180101", 800, height, "Station {} : 1 depuis le début".format(code))
        gen_graph(os.path.join(png_path, "{}_1day_small.png".format(code)), filename_absolute, "end-1d", 400, 200, "Station {} : 1 jour".format(code))
